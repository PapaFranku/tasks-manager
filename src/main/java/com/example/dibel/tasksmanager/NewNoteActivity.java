package com.example.dibel.tasksmanager;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

public class NewNoteActivity extends AppCompatActivity
{
    private Note editNote;
    private EditText etName;
    private EditText etContent;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_note);

        //Configure the toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarNewNote);
        toolbar.setTitle("New Note");
        toolbar.setNavigationIcon(R.drawable.back_arrow);
        toolbar.setNavigationOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                onBackPressed();
            }
        });

        etName = (EditText) findViewById(R.id.etNoteName);
        etContent = (EditText) findViewById(R.id.etNoteContent);
        ImageButton imageButton = (ImageButton) findViewById(R.id.btnDeleteNote);
        imageButton.setVisibility(View.INVISIBLE);

        //Get the note object if there is any
        editNote = getIntent().getParcelableExtra("note");

        //If there is a note object passed, show the menu with additional options
        if(editNote != null)
        {
            toolbar.setTitle("Edit Note");
            imageButton.setVisibility(View.VISIBLE);
            imageButton.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    NotesManager manager = new NotesManager(NewNoteActivity.this);
                    manager.Open();

                    manager.DeleteNote(editNote);

                    manager.Close();
                    finish();
                }
            });

            etName.setText(editNote.GetName());
            etContent.setText(editNote.GetContent());
        }

        Button button = (Button) findViewById(R.id.btnSaveNote);
        button.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                String noteName;

                noteName = etName.getText().toString();

                //If the Edit Text is empty return
                if(TextUtils.isEmpty(noteName))
                {
                    etName.setError("You must enter a name for your note");
                    return;
                }

                NotesManager manager = new NotesManager(NewNoteActivity.this);
                manager.Open();

                if(editNote == null)
                {
                    //Create a new note
                    manager.CreateNote(noteName, etContent.getText().toString());
                }
                else
                {
                    //Update an existing note
                    editNote.SetName(noteName);
                    editNote.SetContent(etContent.getText().toString());

                    manager.UpdateNote(editNote);
                }

                manager.Close();
                finish();
            }
        });
    }
}
