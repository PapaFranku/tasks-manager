package com.example.dibel.tasksmanager;

public enum TimerState
{
    RUNNING, PAUSED, STOPPED
}
