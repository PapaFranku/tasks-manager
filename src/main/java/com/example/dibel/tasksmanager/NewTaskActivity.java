package com.example.dibel.tasksmanager;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class NewTaskActivity extends AppCompatActivity
{
    private Task editTask;
    private EditText etTaskName;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_task);

        //Configure the toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarNewTask);
        toolbar.setTitle("New Task");
        toolbar.setNavigationIcon(R.drawable.back_arrow);
        toolbar.setNavigationOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                onBackPressed();
            }
        });

        etTaskName = (EditText) findViewById(R.id.etTaskName);

        //Get the task object if there is any
        editTask = getIntent().getParcelableExtra("task");

        //If there is a task object passed, show the menu with additional options
        if(editTask != null)
        {
            toolbar.setTitle("Edit Task");
            toolbar.inflateMenu(R.menu.menu_task_edit);
            toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener()
            {
                @Override
                public boolean onMenuItemClick(MenuItem item)
                {
                    int id = item.getItemId();

                    if (id == R.id.action_deleteTask)
                    {
                        TasksManager manager = new TasksManager(NewTaskActivity.this);
                        manager.Open();

                        manager.DeleteTask(editTask);

                        manager.Close();

                        finish();
                        return true;
                    }
                    else if(id == R.id.action_resetTime)
                    {
                        editTask.ResetTime();
                        return true;
                    }

                    return false;
                }
            });
            etTaskName.setText(editTask.GetName());
        }

        Button btn = (Button) findViewById(R.id.btnSaveTask);
        btn.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                String taskName;

                taskName = etTaskName.getText().toString();

                //If the Edit Text is empty return
                if(TextUtils.isEmpty(taskName))
                {
                    etTaskName.setError("You must enter a name for your task");
                    return;
                }

                TasksManager manager = new TasksManager(NewTaskActivity.this);
                manager.Open();

                if(editTask == null)
                {
                    //Create a new task
                    manager.CreateTask(taskName);
                }
                else
                {
                    //Update an existing task
                    editTask.SetName(taskName);
                    manager.UpdateTask(editTask);
                }

                manager.Close();
                finish();
            }
        });
    }
}
