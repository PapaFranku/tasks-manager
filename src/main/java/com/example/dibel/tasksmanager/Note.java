package com.example.dibel.tasksmanager;


import android.os.Parcel;
import android.os.Parcelable;

public class Note implements Parcelable
{
    private int id;
    private String name;
    private String content;

    public Note(String name, String content, int id)
    {
        this.name = name;
        this.content = content;
        this.id = id;
    }

    private Note(Parcel in)
    {
        this.name = in.readString();
        this.content = in.readString();
        this.id = in.readInt();
    }

    public int GetId()
    {
        return this.id;
    }

    public String GetName()
    {
        return this.name;
    }

    public void SetName(String name)
    {
        this.name = name;
    }

    public String GetContent() { return this.content; }

    public void SetContent(String content) { this.content = content; }


    //Methods for the interface
    @Override
    public int describeContents() { return 0; }

    @Override
    public void writeToParcel(Parcel out, int flags)
    {
        out.writeString(name);
        out.writeString(content);
        out.writeInt(id);
    }

    public static final Parcelable.Creator<Note> CREATOR = new Parcelable.Creator<Note>()
    {
        public Note createFromParcel(Parcel in)
        {
            return new Note(in);
        }

        public Note[] newArray(int size)
        {
            return new Note[size];
        }
    };
}
