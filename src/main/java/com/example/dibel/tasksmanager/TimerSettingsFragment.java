package com.example.dibel.tasksmanager;

import android.os.Bundle;
import android.preference.PreferenceFragment;


public class TimerSettingsFragment extends PreferenceFragment
{
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        //Get the widget layout
        addPreferencesFromResource(R.xml.timer_preferences);
    }
}
