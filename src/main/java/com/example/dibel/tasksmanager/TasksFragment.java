package com.example.dibel.tasksmanager;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;


public class TasksFragment extends Fragment
{
    private TasksListAdapter tasksAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceBundle)
    {
        View view = inflater.inflate(R.layout.tasks_fragment, container, false);

        FloatingActionButton fab = (FloatingActionButton) view.findViewById(R.id.btnAddTask);
        fab.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent newIntent = new Intent(getActivity(), NewTaskActivity.class );
                startActivity(newIntent);
            }
        });

        TasksManager manager = new TasksManager(this.getActivity());
        manager.Open();

        ListView listView = (ListView) view.findViewById(R.id.lvTasks);
        tasksAdapter = new TasksListAdapter(getActivity(), manager.GetAllTasks());
        listView.setAdapter(tasksAdapter);
        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener()
        {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id)
            {
                Intent newIntent = new Intent(getActivity(), NewTaskActivity.class);
                Task task = (Task) parent.getItemAtPosition(position);

                newIntent.putExtra("task", task);
                startActivity(newIntent);

                return true;
            }
        });

        manager.Close();
        return view;
    }

    @Override
    public void onResume()
    {
        super.onResume();

        //Update the UI
        TasksManager manager = new TasksManager(this.getActivity());
        manager.Open();

        tasksAdapter.AddTasks(manager.GetAllTasks());
        tasksAdapter.notifyDataSetChanged();
        manager.Close();
    }
}
