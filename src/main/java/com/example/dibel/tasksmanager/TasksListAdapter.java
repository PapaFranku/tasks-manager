package com.example.dibel.tasksmanager;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.Locale;
import java.util.concurrent.TimeUnit;


public class TasksListAdapter extends BaseAdapter
{
    private ArrayList<Task> tasks;
    private LayoutInflater layoutInflater;
    private Context context;

    public TasksListAdapter(Context context, ArrayList<Task> tasks)
    {
        this.layoutInflater = LayoutInflater.from(context);
        this.context = context;
        this.tasks = tasks;
    }

    public void AddTasks(ArrayList<Task> tasks)
    {
        this.tasks = tasks;
    }

    @Override
    public int getCount()
    {
        return tasks.size();
    }

    @Override
    public long getItemId(int position)
    {
        return position;
    }

    @Override
    public Task getItem(int position)
    {
        return tasks.get(position);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent)
    {
        convertView = layoutInflater.inflate(R.layout.task_object, null);

        TextView textView = (TextView) convertView.findViewById(R.id.tvTaskName);
        textView.setText(tasks.get(position).GetName());

        TextView textViewTime = (TextView) convertView.findViewById(R.id.tvTaskSpentTime);
        //Convert the time in minutes to HH:MM format string
        long time = tasks.get(position).GetSpentTime();
        textViewTime.setText(formatTime(time));

        ImageButton imgBtn = (ImageButton) convertView.findViewById(R.id.btnStart);
        imgBtn.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                //Start the TimerActivity
                Intent newIntent = new Intent(context, TimerActivity.class );
                newIntent.putExtra("task", tasks.get(position));
                context.startActivity(newIntent);
            }
        });

        return convertView;
    }

    private String formatTime(long mins)
    {
        int hours = (int) mins / 60;
        int minutes = (int) mins % 60;

        return String.format(Locale.getDefault(), "%dh %02dmin", hours, minutes);
    }
}
