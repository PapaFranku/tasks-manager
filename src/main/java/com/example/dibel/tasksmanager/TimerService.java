package com.example.dibel.tasksmanager;

import android.app.Service;
import android.content.Intent;

import java.util.Locale;
import java.util.concurrent.TimeUnit;

import android.content.SharedPreferences;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.CountDownTimer;
import android.support.v4.os.ResultReceiver;
import android.preference.PreferenceManager;
import android.widget.Toast;

public class TimerService extends Service
{
    public static final String RECEIVER = "receiver";
    public static final String KEY_TIME = "time";
    public static final String KEY_SESSION_NUMBER = "session_number";
    public static final String KEY_STATE = "state";

    public class TimerBinder extends Binder
    {
        TimerService getTimer()
        {
            return TimerService.this;
        }
    }

    private long currentTime;
    private long totalWorkingTime = 1000;
    private final IBinder binder = new TimerBinder();
    private CountDownTimer cdTimer;
    private TimerState timerState = TimerState.STOPPED;
    private SessionState sessionState = SessionState.WORK;
    private int sessionNumber = 1;
    private ResultReceiver receiver;
    private Bundle sendBundle;

    @Override
    public IBinder onBind(Intent intent)
    {
        Bundle bundle = intent.getExtras();
        receiver = bundle.getParcelable(RECEIVER);

        return binder;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId)
    {
        return START_NOT_STICKY;
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();

        //Reset the timer for next session
        sessionState = SessionState.WORK;
        currentTime = 0;
        totalWorkingTime = 1000;
        sessionNumber = 1;
        StopTimer();
        stopSelf();
    }

    public void StartTimer()
    {
        if (timerState == TimerState.RUNNING)
        {
            //Stop the timer
            timerState = TimerState.PAUSED;
            cdTimer.cancel();
        }
        else if (timerState == TimerState.PAUSED)
        {
            //If it is passed start with the last time
            timerState = TimerState.RUNNING;
            setUpTimer(currentTime);
            cdTimer.start();
        }
        else if (timerState == TimerState.STOPPED)
        {
            long time;
            SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(this);

            //Configure the timer, depending on which state the session is in
            switch (sessionState)
            {
                case WORK:
                    time = TimeUnit.MINUTES.toMillis(pref.getInt("sessionTime", 30));
                    break;
                case BREAK:
                    time = TimeUnit.MINUTES.toMillis(pref.getInt("breakTime", 30));
                    break;
                default:
                    time = TimeUnit.MINUTES.toMillis(pref.getInt("longBreakTime", 30));
                    break;
            }

            timerState = TimerState.RUNNING;

            setUpTimer(time);
            cdTimer.start();
        }
    }

    public void StopTimer()
    {
        if(cdTimer != null)
        {
            cdTimer.cancel();
            timerState = TimerState.STOPPED;
        }
    }

    public void ResetSession()
    {
        //Reset the sessions
        this.sessionNumber = 1;
        StopTimer();
        sessionState = SessionState.WORK;
        sendState();
    }

    public long GetTotalWorkingTime()
    {
        return totalWorkingTime;
    }

    private String getTimeString()
    {
        //Convert the time to a formatted string
        return String.format(Locale.getDefault(), "%02d:%02d",
                TimeUnit.MILLISECONDS.toMinutes(currentTime),
                TimeUnit.MILLISECONDS.toSeconds(currentTime) -
                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(currentTime)));
    }

    private void setUpTimer(long amount)
    {
        cdTimer = new CountDownTimer(amount, 1000)
        {
            @Override
            public void onTick(long millisUntilFinished)
            {
                //Send a message to the activity with the current time
                currentTime = millisUntilFinished;
                sendBundle = new Bundle();
                sendBundle.putString(KEY_TIME, getTimeString());
                receiver.send(TimerActivity.MSG_TIME_UPDATE, sendBundle);

                if(sessionState == SessionState.WORK)
                {
                    //If in working state, add to the total time, spent working
                    totalWorkingTime += 1000;
                }
            }

            @Override
            public void onFinish()
            {
                //Configure the timer according to the session
                if (sessionState == SessionState.WORK)
                {
                    startBreak();
                    totalWorkingTime += 1000;
                }
                else
                {
                    startWork();
                }

                //Send a message to the activity with the new informaton
                sendState();
            }
        };
    }

    private void startBreak()
    {
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(this);
        long time;

        if (sessionNumber < 4)
        {
            //Configure for short break
            Toast.makeText(getBaseContext(), "Short Break", Toast.LENGTH_LONG).show();
            time = TimeUnit.MINUTES.toMillis(pref.getInt("breakTime", 5));
            sessionState = SessionState.BREAK;
            sessionNumber++;
        }
        else
        {
            //Configure for long break
            Toast.makeText(getBaseContext(), "Long Break", Toast.LENGTH_LONG).show();
            time = TimeUnit.MINUTES.toMillis(pref.getInt("longBreakTime", 15));
            sessionState = SessionState.LONG_BREAK;
            sessionNumber = 1;
        }

        StopTimer();
        setUpTimer(time);
        StartTimer();
    }

    private void startWork()
    {
        //Configure for work
        Toast.makeText(getBaseContext(), "Work", Toast.LENGTH_LONG).show();
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(this);
        long time = TimeUnit.MINUTES.toMillis(pref.getInt("sessionTime", 30));

        sessionState = SessionState.WORK;

        StopTimer();
        setUpTimer(time);
        StartTimer();
    }

    private void sendState()
    {
        sendBundle = new Bundle();
        sendBundle.putInt(KEY_SESSION_NUMBER, sessionNumber);
        sendBundle.putSerializable(KEY_STATE, sessionState);
        receiver.send(TimerActivity.MSG_STATE, sendBundle);
    }
}