package com.example.dibel.tasksmanager;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

public class TasksManager
{
    private SQLiteDatabase database;
    private TasksDatabase taskHelper;
    private String[] allColumns = { TasksDatabase.COLUMN_ID, TasksDatabase.COLUMN_NAME,
        TasksDatabase.COLUMN_TIME_SPENT};

    public TasksManager(Context context)
    {
        this.taskHelper = new TasksDatabase(context);
    }

    public void Open() throws SQLException
    {
        database = taskHelper.getWritableDatabase();
    }

    public void Close() {
        taskHelper.close();
    }

    public Task CreateTask(String taskName)
    {
        ContentValues values = new ContentValues();
        values.put(TasksDatabase.COLUMN_NAME, taskName);
        values.put(TasksDatabase.COLUMN_TIME_SPENT, (long)0);

        long insertId = database.insert(TasksDatabase.TABLE_TASKS, null, values);

        Cursor cursor = database.query(TasksDatabase.TABLE_TASKS, allColumns, TasksDatabase.COLUMN_ID +
            " = " + insertId, null, null, null, null);

        cursor.moveToFirst();
        Task task = cursorToTask(cursor);
        cursor.close();

        return task;
    }

    public void DeleteTask(Task task)
    {
        database.delete(TasksDatabase.TABLE_TASKS, TasksDatabase.COLUMN_ID
                + " = " + task.GetId(), null);
    }

    public void UpdateTask(Task task)
    {
        ContentValues values = new ContentValues();
        values.put(TasksDatabase.COLUMN_NAME, task.GetName());
        values.put(TasksDatabase.COLUMN_TIME_SPENT, task.GetSpentTime());

        database.update(TasksDatabase.TABLE_TASKS, values,
               TasksDatabase.COLUMN_ID + " = " + task.GetId(), null);
    }

    public ArrayList<Task> GetAllTasks()
    {
        ArrayList<Task> tasks = new ArrayList<Task>();
        Cursor cursor = database.query(taskHelper.TABLE_TASKS, allColumns, null, null, null, null, null);
        cursor.moveToFirst();

        while(cursor.isAfterLast() == false)
        {
            tasks.add(cursorToTask(cursor));
            cursor.moveToNext();
        }

        cursor.close();
        return tasks;
    }

    private Task cursorToTask(Cursor cursor)
    {
        //Construct a task object
        int index_id = cursor.getColumnIndex(TasksDatabase.COLUMN_ID);
        int index_name = cursor.getColumnIndex(TasksDatabase.COLUMN_NAME);
        int index_time = cursor.getColumnIndex(TasksDatabase.COLUMN_TIME_SPENT);
        Task task = new Task(cursor.getString(index_name), cursor.getInt(index_id));
        task.AddTime(cursor.getLong(index_time));
        return task;
    }
}
