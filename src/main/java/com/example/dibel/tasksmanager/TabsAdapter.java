package com.example.dibel.tasksmanager;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

public class TabsAdapter extends FragmentPagerAdapter
{
    public TabsAdapter(FragmentManager manager)
    {
        super(manager);
    }

    @Override
    public Fragment getItem(int index)
    {
        switch(index)
        {
            case 0:
                return new TasksFragment();
            case 1:
                return new NotesFragment();
        }

        return null;
    }

    @Override
    public int getCount()
    {
        return 2;
    }
}
