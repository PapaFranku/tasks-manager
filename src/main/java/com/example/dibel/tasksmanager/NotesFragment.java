package com.example.dibel.tasksmanager;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

public class NotesFragment extends Fragment
{
    private NotesListAdapter notesAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
        Bundle savedInstanceBundle)
    {
        View view = inflater.inflate(R.layout.notes_fragment, container, false);

        FloatingActionButton fab = (FloatingActionButton) view.findViewById(R.id.btnAddNote);
        fab.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent newIntent = new Intent(getActivity(), NewNoteActivity.class );
                startActivity(newIntent);
            }
        });

        NotesManager manager = new NotesManager(this.getActivity());
        manager.Open();

        ListView listView = (ListView) view.findViewById(R.id.lvNotes);
        notesAdapter = new NotesListAdapter(getActivity(), manager.GetAllNotes());
        listView.setAdapter(notesAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                Intent newIntent = new Intent(getActivity(), NewNoteActivity.class);
                Note note = (Note) parent.getItemAtPosition(position);

                newIntent.putExtra("note", note);
                startActivity(newIntent);
            }
        });

        manager.Close();
        return view;
    }

    @Override
    public void onResume()
    {
        super.onResume();

        //Update the UI
        NotesManager manager = new NotesManager(this.getActivity());
        manager.Open();

        notesAdapter.AddNotes(manager.GetAllNotes());
        notesAdapter.notifyDataSetChanged();
        manager.Close();
    }
}
