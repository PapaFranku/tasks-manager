package com.example.dibel.tasksmanager;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class TasksDatabase extends SQLiteOpenHelper
{
    public static final String TABLE_TASKS = "tasks";
    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_TIME_SPENT = "time_spent";

    private static final String DATABASE_NAME = "db.tasks";
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_CREATE = "CREATE TABLE " + TABLE_TASKS + "( " +
                                                    COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                                                    COLUMN_NAME + " TEXT NOT NULL, " +
                                                    COLUMN_TIME_SPENT + " INTEGER NOT NULL)";

    public TasksDatabase(Context context)
    {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase database) {
        database.execSQL(DATABASE_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_TASKS);
        onCreate(db);
    }
}
