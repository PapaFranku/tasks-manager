package com.example.dibel.tasksmanager;

public enum SessionState
{
    WORK, BREAK, LONG_BREAK
}
