package com.example.dibel.tasksmanager;


import android.os.Parcel;
import android.os.Parcelable;

public class Task implements Parcelable
{
    private int id;
    private String name;
    private long spentTime;

    public Task(String name, int id)
    {
        this.name = name;
        this.id = id;
        this.spentTime = 0;
    }

    private Task(Parcel in)
    {
        this.name = in.readString();
        this.id = in.readInt();
        this.spentTime = in.readLong();
    }

    public int GetId()
    {
        return this.id;
    }

    public String GetName()
    {
        return this.name;
    }

    public void SetName(String name)
    {
        this.name = name;
    }

    public long GetSpentTime()
    {
        return this.spentTime;
    }

    public void AddTime(long time)
    {
        this.spentTime += time;
    }

    public void ResetTime()
    {
        this.spentTime = 0;
    }

    //Methods for the interface
    @Override
    public int describeContents() { return 0; }

    @Override
    public void writeToParcel(Parcel out, int flags)
    {
        out.writeString(name);
        out.writeInt(id);
        out.writeLong(spentTime);
    }

    public static final Parcelable.Creator<Task> CREATOR = new Parcelable.Creator<Task>()
    {
        public Task createFromParcel(Parcel in)
        {
            return new Task(in);
        }

        public Task[] newArray(int size)
        {
            return new Task[size];
        }
    };
}
