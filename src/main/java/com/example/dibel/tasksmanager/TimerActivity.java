package com.example.dibel.tasksmanager;

import android.app.KeyguardManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;

import java.util.concurrent.TimeUnit;

import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.PowerManager;
import android.os.Vibrator;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v4.os.ResultReceiver;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

public class TimerActivity extends AppCompatActivity
{
    public static final int MSG_TIME_UPDATE = 0;
    public static final int MSG_STATE = 1;

    private Task currentTask;
    private TextView sessionText;
    private TextView timeText;
    private TextView sessionNumberText;
    private FloatingActionButton btnPlay;
    private SoundPool soundPool;
    private int soundWork;
    private int soundBreak;
    private TimerService timerService;
    private boolean serviceIsBound;
    private boolean isPaused = true;
    private ServiceConnection connection = new ServiceConnection()
    {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service)
        {
            //Bind the timer
            TimerService.TimerBinder binder = (TimerService.TimerBinder) service;
            timerService = binder.getTimer();
            serviceIsBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName name)
        {

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_timer);

        //Set up the toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Timer");
        toolbar.inflateMenu(R.menu.menu_timer);
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener()
        {
            @Override
            public boolean onMenuItemClick(MenuItem item)
            {
                int id = item.getItemId();
                if(id == R.id.action_resetSessions)
                {
                    sessionNumberText.setText("Session 1");

                    if(serviceIsBound == true)
                    {
                        //Reset the session
                        timerService.ResetSession();
                        btnPlay.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(),
                                R.drawable.play_arrow));
                        isPaused = true;
                        timeText.setText("00:00");
                    }
                }

                return false;
            }
        });
        toolbar.setNavigationIcon(R.drawable.back_arrow);
        toolbar.setNavigationOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                onBackPressed();
            }
        });

        //Get the task object associated with the activity
        currentTask = getIntent().getParcelableExtra("task");

        ImageButton btnSetting = (ImageButton) findViewById(R.id.btnTimerSettings);
        btnSetting.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                //Start the TimerSettingsActivity
                Intent newIntent = new Intent(getBaseContext(), TimerSettingsActivity.class );
                startActivity(newIntent);
            }
        });

        sessionNumberText = (TextView) findViewById(R.id.tvSessionNum);
        timeText = (TextView) findViewById(R.id.tvTime);
        sessionText = (TextView) findViewById(R.id.tvSessionState);
        TextView taskName = (TextView) findViewById(R.id.tvTaskNameTimer);
        taskName.setText(currentTask.GetName());

        btnPlay = (FloatingActionButton) findViewById(R.id.btnPlay);
        btnPlay.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if(serviceIsBound == true)
                {
                    //Change the drawable accordingly
                    if(isPaused == true)
                    {
                        btnPlay.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(),
                                R.drawable.ic_pause_black_24dp));
                        isPaused = false;
                    }
                    else
                    {
                        btnPlay.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(),
                                R.drawable.play_arrow));
                        isPaused = true;
                    }

                    timerService.StartTimer();
                }
            }
        });

        FloatingActionButton btnStop = (FloatingActionButton) findViewById(R.id.btnStop);
        btnStop.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if(serviceIsBound == true)
                {
                    //Reset the time and change the play button drawable to "Play"
                    if(isPaused == false)
                    {
                        btnPlay.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(),
                                R.drawable.play_arrow));
                        isPaused = true;
                    }

                    timerService.StopTimer();
                }
            }
        });

        bindTimerService();
        loadSounds();
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        stopService(new Intent(this, TimerService.class));
        unbindTimerService();
    }

    @Override
    protected void onPause()
    {
        super.onPause();

        //Update the task in the database
        TasksManager manager = new TasksManager(this);
        manager.Open();

        long time = TimeUnit.MILLISECONDS.toMinutes(timerService.GetTotalWorkingTime());
        currentTask.AddTime(time);
        manager.UpdateTask(currentTask);

        manager.Close();
    }

    private void bindTimerService()
    {
        Intent newIntent = new Intent(this, TimerService.class);
        newIntent.putExtra(TimerService.RECEIVER, new ResultReceiver(new Handler())
        {
            //The TimerService uses this to send messages
            @Override
            protected void onReceiveResult(int resultCode, Bundle resultData)
            {
                if(resultCode == MSG_TIME_UPDATE)
                {
                    //Update the time text
                    timeText.setText(resultData.getString(TimerService.KEY_TIME));
                }
                else if(resultCode == MSG_STATE)
                {
                    SessionState state = (SessionState) resultData.getSerializable(TimerService.KEY_STATE);
                    sessionText.setText(state.toString());

                    if(state == SessionState.WORK)
                    {
                        //Update the session number
                        String sessionNum = "Session " + resultData.getInt(TimerService.KEY_SESSION_NUMBER);
                        sessionNumberText.setText(sessionNum);
                    }

                    //Play a sound according to witch state the session is in
                    //Wake up the screen
                    playSound(state);
                }
            }
        });

        bindService(newIntent, connection, Context.BIND_AUTO_CREATE);
        serviceIsBound = true;
    }

    private void unbindTimerService()
    {
        if(serviceIsBound == true)
        {
            unbindService(connection);
            serviceIsBound = false;
        }
    }

    private void loadSounds()
    {
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
        {
            soundPool = new SoundPool.Builder()
                    .setMaxStreams(2)
                    .build();
        }
        else
        {
            //Use old constructor for lower API
            soundPool = new SoundPool(2, AudioManager.STREAM_MUSIC, 1);
        }

        //Save the IDs to variables
        soundWork = soundPool.load(this, R.raw.demonstrative, 1);
        soundBreak = soundPool.load(this, R.raw.the_little_dwarf, 1);
    }

    private void playSound(SessionState state)
    {
        AudioManager manager = (AudioManager) getSystemService(AUDIO_SERVICE);

        //Get the actual volume
        float currentVolume = (float) manager.getStreamVolume(AudioManager.STREAM_MUSIC);
        float maxVolume = (float) manager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
        float actualVolume = currentVolume / maxVolume;

        if(state == SessionState.WORK)
        {
            //Play work sound
            soundPool.play(soundWork, actualVolume, actualVolume, 1, 0, 1f);
        }
        else
        {
            //Play break sound
            soundPool.play(soundBreak, actualVolume, actualVolume, 1, 0, 1f);
        }

        //Vibrate
        Vibrator vibrator = (Vibrator) this.getSystemService(VIBRATOR_SERVICE);
        vibrator.vibrate(500);

        wakeScreen();
    }

    private void wakeScreen()
    {
        //Wake the screen
        PowerManager manager = (PowerManager) this.getSystemService(POWER_SERVICE);
        PowerManager.WakeLock wakeLock = manager.newWakeLock((PowerManager.FULL_WAKE_LOCK |
                PowerManager.ACQUIRE_CAUSES_WAKEUP), "wake");

        //Disable the keyguard
        KeyguardManager keyguardManager = (KeyguardManager) this.getSystemService(Context.KEYGUARD_SERVICE);
        KeyguardManager.KeyguardLock keyguardLock = keyguardManager.newKeyguardLock("wake");

        keyguardLock.disableKeyguard();
        wakeLock.acquire();
    }
}
