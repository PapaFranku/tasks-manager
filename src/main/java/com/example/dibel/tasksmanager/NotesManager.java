package com.example.dibel.tasksmanager;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

public class NotesManager
{
    private SQLiteDatabase database;
    private NotesDatabase notesHelper;
    private String[] allColumns = { NotesDatabase.COLUMN_ID, NotesDatabase.COLUMN_NAME,
            NotesDatabase.COLUMN_CONTENT};

    public NotesManager(Context context)
    {
        this.notesHelper = new NotesDatabase(context);
    }

    public void Open() throws SQLException
    {
        database = notesHelper.getWritableDatabase();
    }

    public void Close() {
        notesHelper.close();
    }

    public Note CreateNote(String taskName, String content)
    {
        ContentValues values = new ContentValues();
        values.put(NotesDatabase.COLUMN_NAME, taskName);
        values.put(NotesDatabase.COLUMN_CONTENT, content);

        long insertId = database.insert(NotesDatabase.TABLE_NOTES, null, values);

        Cursor cursor = database.query(NotesDatabase.TABLE_NOTES, allColumns, NotesDatabase.COLUMN_ID +
                " = " + insertId, null, null, null, null);

        cursor.moveToFirst();
        Note note = cursorToNote(cursor);
        cursor.close();

        return note;
    }

    public void DeleteNote(Note note)
    {
        database.delete(NotesDatabase.TABLE_NOTES, NotesDatabase.COLUMN_ID
                + " = " + note.GetId(), null);
    }

    public void UpdateNote(Note note)
    {
        ContentValues values = new ContentValues();
        values.put(NotesDatabase.COLUMN_NAME, note.GetName());
        values.put(NotesDatabase.COLUMN_CONTENT, note.GetContent());

        database.update(NotesDatabase.TABLE_NOTES, values,
                NotesDatabase.COLUMN_ID + " = " + note.GetId(), null);
    }

    public ArrayList<Note> GetAllNotes()
    {
        ArrayList<Note> notes = new ArrayList<Note>();
        Cursor cursor = database.query(NotesDatabase.TABLE_NOTES, allColumns, null, null, null, null, null);
        cursor.moveToFirst();

        while(cursor.isAfterLast() == false)
        {
            notes.add(cursorToNote(cursor));
            cursor.moveToNext();
        }

        cursor.close();
        return notes;
    }

    private Note cursorToNote(Cursor cursor)
    {
        //Construct a note object
        int index_id = cursor.getColumnIndex(NotesDatabase.COLUMN_ID);
        int index_name = cursor.getColumnIndex(NotesDatabase.COLUMN_NAME);
        int index_content = cursor.getColumnIndex(NotesDatabase.COLUMN_CONTENT);
        Note note = new Note(cursor.getString(index_name), cursor.getString(index_content),
                cursor.getInt(index_id));

        return note;
    }
}
