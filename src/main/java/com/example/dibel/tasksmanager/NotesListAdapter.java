package com.example.dibel.tasksmanager;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class NotesListAdapter extends BaseAdapter
{
    private ArrayList<Note> notes;
    private LayoutInflater inflater;

    public NotesListAdapter(Context context, ArrayList<Note> notes)
    {
        this.notes = notes;
        this.inflater = LayoutInflater.from(context);
    }

    public void AddNotes(ArrayList<Note> notes) { this.notes = notes; }

    @Override
    public int getCount()
    {
        return notes.size();
    }

    @Override
    public long getItemId(int position)
    {
        return position;
    }

    @Override
    public Note getItem(int position) { return notes.get(position); }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent)
    {
        //Construct the note item
        convertView = inflater.inflate(R.layout.note_object, null);

        TextView tvName = (TextView) convertView.findViewById(R.id.tvNoteName);
        tvName.setText(notes.get(position).GetName());

        TextView tvContent = (TextView) convertView.findViewById(R.id.tvNoteContent);
        tvContent.setText(notes.get(position).GetContent());

        return convertView;
    }
}
